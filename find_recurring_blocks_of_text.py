#!/usr/bin/env python
"""Find repeating text blocks in files."""

from sys import exit as sysexit
import argparse
from collections import defaultdict
from typing import NamedTuple, Iterable


def main():
    """Parse commandline arguments, search files, print output."""
    args = parse_commandline_arguments()

    # gather blocks
    lists_of_blocks = (
        find_blocks_of_given_length_in_file(block_length, file)
        for file in args.files_to_search
        for block_length in range(
                args.min_block_length,
                args.max_block_length + 1
        )
    )
    all_blocks = compress_list_of_blocks(sum(lists_of_blocks, []))

    # find the most interesting blocks among them
    relevant_blocks = find_relevant_blocks(
        all_blocks,
        args.min_number_of_occurences
    )
    interesting_blocks = find_most_interesting_blocks(relevant_blocks)

    # output
    for block in interesting_blocks:
        print_block(block)


class TextReference(NamedTuple):
    """A reference to a line in a file."""
    file_name: str
    line_number: int


class Block(NamedTuple):
    """The references mark the first lines of occurences of the text_block."""

    text_block: tuple[str, ...]
    references: set[TextReference]


def compress_list_of_blocks(list_of_blocks: Iterable[Block]) -> list[Block]:
    """Take lists of blocks and combine them into one.

    This is not trivial because
    """
    block_dict = defaultdict(set)

    for block in list_of_blocks:
        block_dict[block.text_block].update(block.references)
    return [
        Block(text_block=text_block, references=references)
        for text_block, references
        in block_dict.items()
    ]


def find_blocks_of_given_length_in_file(
        block_length: int,
        file_name: str
) -> list[Block]:
    """Return a dict of all text blocks.

    The result is will not be compressed, yet.
    This means that there might be blocks in there that could be combined,
    because their text_block is equal and they only differ in the references
    """
    with open(file_name, encoding="utf8") as file:
        lines = [
            line.rstrip()  # remove trailing whitespace and newline
            for line
            in file.readlines()
        ]
    return [
        Block(
            text_block=tuple(
                lines[start_line_index:(start_line_index+block_length)]
            ),
            references=set([TextReference(
                file_name=file_name,
                line_number=start_line_index,
            )]),
        )
        for start_line_index
        in range(len(lines) - block_length + 1)
    ]


def find_relevant_blocks(
        blocks: Iterable[Block],
        min_number_of_occurences: int
) -> Iterable[Block]:
    """Return only blocks that fullfill the requirements."""
    return (
        block for block in blocks
        if min_number_of_occurences <= len(block.references)
    )


def find_most_interesting_blocks(blocks: Iterable[Block]) -> Iterable[Block]:
    """Find the biggest blocks with the most references."""
    blocks = list(blocks)  # we will iterate multiple times through the blocks

    # Break early if there are no blocks that match the criteria.
    if not blocks:
        sysexit("No text blocks were found that match the given criteria. "
                "Try --help to see a list of options.")

    highest_interest_score = max(
        interest_score(block)
        for block
        in blocks
    )
    blocks_of_highest_interest = (
        block
        for block
        in blocks
        if interest_score(block) == highest_interest_score
    )
    return blocks_of_highest_interest


def interest_score(block: Block) -> tuple[int, int]:
    """Tell how interesting a block is.

    The result is sortable so it can be used as a key for min/max/sorted etc.
    """
    return (
        pow(len(block.text_block), 2) * len(block.references),
        len(block.text_block)
    )


def print_block(block: Block) -> None:
    """Prettyprint a block."""
    text_block, references = block
    print(f"A block of length {len(text_block)} "
          f"with {len(references)} references.")
    for reference in sorted(references):
        print(f"{reference.file_name}:{reference.line_number}")
    for line in text_block:
        print(f"> {line}")


def parse_commandline_arguments():
    """Parse the arguments the progamm was called with into a dict.

    If appropeate, print out help information.
    """
    parser = argparse.ArgumentParser(
        description="Find recurring text_blocks in files.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--min_block_length",
        "-l",
        type=int,
        default=1,
        help="The minimum number of lines a block has to have."
    )
    parser.add_argument(
        "--max_block_length",
        "-L",
        type=int,
        default=50,
        help="The maximum number of lines a block has to have."
    )
    parser.add_argument(
        "--min_number_of_occurences",
        "-o",
        type=int,
        default=2,
        help="The minimum number of occurences a block has to have. "
        "Occurences are counted over all the searhced files combined. "
        "Blocks with too few occurences will not be considered for analysis."
    )
    parser.add_argument(
        "files_to_search",
        nargs="+",
        help="The file(s) to find textblocks in."
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
